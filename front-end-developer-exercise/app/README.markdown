The 7 Baby Steps
=================
##Getting Started

This project build a web page being constructed only by pure javascript and html, css that will provide information about The 7 Baby Steps.

##Prerequisites

This package is configured with a basic web server. You'll first need to install [Node.js](http://nodejs.org/). Once you have Node.js installed and have cloned this repo, then you can start the server (on port 8080 by default).

##Installation

Do the following steps:
```sh
Download and install Node: https://nodejs.org/
Clone this repo: git clone https://dungnguyent8@bitbucket.org/dungnguyent8/ex.git (HTTPS) or git clone git@bitbucket.org:dungnguyent8/ex.git (SSH).
Install project dependancies: npm install
Start the development environment: npm start
Open your browser and visit http://localhost:8080.
```

##Project information

There are 7 articles on this page at all.  Each of them has a unique icon pictures and text contents. On each article, or we should call each "step", you're able to see how many your friend have finished or being on that step. And so you can click on their name to go to an external link, maybe this features will be constructed later.

The page seperated into 2 columns, on the left is navbar,which contains 7 buttons (or links) that you can click on it to change the content of the right side of page. And so on, each times you click one button, display zone will be changed to new content, and which article display on this zone will depend on what button you have clicked.

Under each of steps article has a line that shows name of friends have being on that steps. That features made by javascript, ajax with some logic here:  
If you have no one being on that step, you can not see the line under that step.
If you have at least one friends on step, you can see her/his name display with a blue color on names, they can act like an external link, so you can trying click on that to make the browser redirect to another pages. But you can only see names of maximum 2 mens, other will display on notify with format:'n other friends are also on step...'

##Important

If you turn off javascript, almost features using javascript on this page will be disabled (ajax function, transition effects...)

##Languages & tools

Html, css, javascript.

Visual Studio Code + Chrome dev tools.