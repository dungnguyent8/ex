
window.onload = function () {
    setHeadingImgPostion();
    addEventForLink();
    simulateClick();    
}
function setHeadingImgPostion() {
    document.querySelectorAll('.step-element-img').forEach((img, index) => {
        img.style.backgroundPosition = "0" + " -" + (index * 65) + "px";
    })
}
function resetLinksToDefault() {
    document.querySelectorAll('a[href^="#step-"]').forEach((link, num) => {
        a = num + 1;
        link.classList.remove('active');
        link.style.background = 'url(assets/images/icons/individual/icons_small_bs' + a + '.png) 250px no-repeat';
    });
        
}
function addEventForLink() {
    document.querySelectorAll('a[href^="#step-').forEach((link, index) => {
        link.onclick = (e) => {
            // e.preventDefault();
            resetLinksToDefault();
            changeSelectedLink(link, index);
            changeInfo(index);
        }
    });
}
function changeSelectedLink(link, index) {
    link.style.background = 'url(assets/images/icons/individual/icons_small_bs' + (index + 1) + '_blue.png) 250px no-repeat';
    link.classList.add('active');
    let backgroundDiv = document.getElementById('background');
    backgroundDiv.style.top = 62 * index + 'px';    
}

function changeInfo(index) {
    fetch('/baby-steps.json')
        .then(res => res.json())
        .then(data => {
            var friendList = getFriendsList(data.friends, index + 1);
            generateFriendsInfo(friendList, index + 1);
        })

    //change which step is displayed
    let articles=[...document.getElementsByClassName('step-element')];
    articles.forEach((article,num)=>{
        article.classList.remove('display');
        if(num==index) article.classList.add('display');
    })
}


function getFriendsList(friends, step) {
    var friendList = [];
    friends.forEach(friend => {
        if (friend.babyStep == step) {
            friendList.push(friend.firstName + ' ' + friend.lastName);
        }
    });

    return friendList;
}

function generateFriendsInfo(friendList, step) {
    function createLink(num){
        return '<a href="#">'+friendList[num]+'</a>';
    }
    var info = document.getElementById('friend-info');
    console.log(friendList);
    var friendCount = friendList.length;
    if (friendCount == 0) info.innerHTML = '';
    else if (friendCount == 1) info.innerHTML = createLink(0) + ' is also in Baby Step ' + step;
    else if (friendCount == 2) info.innerHTML = createLink(0) + ' and ' + createLink(1) + ' is also in Baby Step ' + step;
    else if (friendCount == 3) info.innerHTML = createLink(0) + ', ' + createLink(1) + ', and 1 other friend are also in Baby Step ' + step;
    else info.innerHTML = createLink(0) + ", " + createLink(1) + ', and ' + (friendCount - 2) + ' other friends are also in Baby Step ' + step;
}
// window.onpopstate= function(){
//     var hash=location.hash.slice(6);
//     document.querySelectorAll('a[href^="#step-')[hash-1].click();
// }



function simulateClick(){
    var hash=location.hash.slice(6);
    document.querySelectorAll('a[href^="#step-')[hash-1].click();
    console.log(document.querySelectorAll('a[href^="#step-')[hash-1])
}


window.onhashchange=()=>{
    simulateClick()
}


